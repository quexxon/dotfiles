#!/bin/bash

try () {
  "$@" > ~/.command_log 2>&1
  local ret_val=$?

  if [ $ret_val -eq 0 ]; then
    echo -e "\e[32mSUCCESS\e[39m"
  else
    echo -e "\e[31mFAILURE\e[39m"
    echo "Command: $@"
    echo "Output:"
    cat ~/.command_log
    exit 1
  fi
}

y_install () {
  echo -n "Installing $1... "
  try yaourt -S --noconfirm --needed $1
}

p_install () {
  echo -n "Installing $1... "
  try sudo pacman -S --noconfirm --needed $1
}

section () {
  echo ""
  echo -e "\e[34m$1\e[39m"
  echo "========================================================================"
}

sudo -v

section "Updating Packages"
echo -n "Refreshing package database... "
try sudo pacman -Syy
echo -n "Updating installed packages... "
try sudo pacman -Syu --noconfirm --needed

section "Installing Packages From Official Arch Repositories"
p_install base-devel
p_install curl
p_install fzf
p_install git
p_install htop
p_install inotify-tools
p_install networkmanager
p_install numlockx
p_install the_silver_searcher
p_install tmux
p_install vim
p_install wget
p_install xclip
p_install zsh

section "Installing Desktop Environment"
p_install xorg-server
p_install xorg-apps
p_install xorg-xinit
p_install gnome-calculator
p_install gnome-terminal
p_install i3-wm
p_install i3blocks
p_install i3status
p_install i3lock
p_install oblogout
p_install dmenu
p_install network-manager-applet
p_install scrot
p_install ttf-dejavu
p_install ttf-hack
p_install compton
p_install mate-polkit

section "Installing Packages From Arch User Repository"
y_install unclutter-xfixes-git
y_install google-chrome

section "Installing Vundle"
VUNDLE_DIR=$HOME/.vim/bundle/Vundle.vim
if [[ -d $VUNDLE_DIR ]]; then
  echo "Vundle is already installed."
  echo -n "Updating Vundle... "
  cd $VUNDLE_DIR
  try git pull
else  
  echo -n "Cloning Vundle repo... "
  try git clone https://github.com/VundleVim/Vundle.vim.git $VUNDLE_DIR
fi

section "Installing Vim Plugins"
echo -n "Running Vundle Install... "
try vim +PluginInstall +qall

section "Installing Base16 Shell"
BASE16_DIR=$HOME/.config/base16-shell
if [[ -d $BASE16_DIR ]]; then
  echo "Base16 Shell is already installed."
  echo -n "Updating Base16 Shell... "
  cd $BASE16_DIR
  try git pull
else
  echo -n "Cloning Base16 Shell repo... "
  try git clone https://github.com/chriskempson/base16-shell.git $BASE16_DIR
fi
echo -n "Running profile_helper.sh... "
try eval "$($BASE16_DIR/profile_helper.sh)"
echo -n "Setting hopscotch colorscheme... "
try sh $BASE16_DIR/scripts/base16-hopscotch.sh
