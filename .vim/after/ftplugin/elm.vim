setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4

let g:elm_make_show_warnings = 1

nnoremap <buffer> <leader>em :<C-u>ElmMake<CR>
nnoremap <buffer> <leader>ef :<C-u>ElmFormat<CR>
nnoremap <buffer> <leader>ee :<C-u>ElmErrorDetail<CR>
nnoremap <buffer> <leader>er :<C-u>ElmRepl<CR>
nnoremap <buffer> <leader>ed :<C-u>ElmShowDocs<CR>
nnoremap <buffer> <leader>eb :<C-u>ElmBrowseDocs<CR>
