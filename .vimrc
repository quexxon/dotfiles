set nocompatible
let mapleader = ' '

"-----------------------------------------------------------------------------
" Vundle Setup
"-----------------------------------------------------------------------------

filetype off

set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'airblade/vim-gitgutter'
Plugin 'alexlafroscia/postcss-syntax.vim'
Plugin 'chriskempson/base16-vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'christoomey/vim-tmux-runner'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'elixir-lang/vim-elixir'
Plugin 'ElmCast/elm-vim'
Plugin 'godlygeek/tabular'
Plugin 'guns/vim-sexp'
Plugin 'kana/vim-textobj-entire'
Plugin 'kana/vim-textobj-user'
Plugin 'mattn/emmet-vim'
Plugin 'mileszs/ack.vim'
Plugin 'mxw/vim-jsx'
Plugin 'nelstrom/vim-textobj-rubyblock'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'scrooloose/nerdtree'
Plugin 'shumphrey/fugitive-gitlab.vim'
Plugin 'thoughtbot/vim-rspec'
Plugin 'tmux-plugins/vim-tmux'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-salve'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-unimpaired'
Plugin 'vim-ruby/vim-ruby'
Plugin 'VundleVim/Vundle.vim'
Plugin 'wavded/vim-stylus'

call vundle#end()

nnoremap <leader>pc :PluginClean<cr>
nnoremap <leader>pi :PluginInstall<cr>
nnoremap <leader>pu :PluginUpdate<cr>

"-----------------------------------------------------------------------------
" Base16 Settings
"-----------------------------------------------------------------------------

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace = 256
  source ~/.vimrc_background
endif

"-----------------------------------------------------------------------------
" Core Settings
"-----------------------------------------------------------------------------

filetype plugin indent on
syntax on
runtime macros/matchit.vim
set updatetime=250
set number

" highlight search results
set hlsearch
highlight Search cterm=None ctermfg=black ctermbg=yellow
nnoremap <silent> <leader>ch :<C-u>nohlsearch<CR>

" Cursor Line Highlighting
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END
"
" wildmenu & wildmode
set wildmenu
set wildmode=full
highlight WildMenu ctermfg=white ctermbg=blue

"-----------------------------------------------------------------------------
" Ack Settings
"-----------------------------------------------------------------------------

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

nnoremap <leader>s :<C-u>Ack!<space>

"-----------------------------------------------------------------------------
" Pane Management
"-----------------------------------------------------------------------------

" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :<C-u>wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :<C-u>wincmd =<cr>

"-----------------------------------------------------------------------------
" Vim Tmux Runner
"-----------------------------------------------------------------------------

nnoremap <leader>vs :VtrSendLinesToRunner<cr>
nnoremap <leader>va :VtrAttachToPane<cr>
nnoremap <leader>vf :VtrSendFile<cr>
nnoremap <leader>vo :VtrOpenRunner {'orientation': 'h', 'percentage': 50}<cr>
nnoremap <leader>fr :VtrFocusRunner<cr>
nnoremap <leader>pry :VtrOpenRunner {'orientation': 'h', 'percentage': 50, 'cmd': 'pry'}<cr>

"-----------------------------------------------------------------------------
" Vim RSpec
"-----------------------------------------------------------------------------

let g:rspec_command = "call VtrSendCommand('rspec {spec}')"
nnoremap <leader>tf :call RunCurrentSpecFile()<CR>
nnoremap <leader>ts :call RunNearestSpec()<CR>
nnoremap <leader>tl :call RunLastSpec()<CR>
nnoremap <leader>ta :call RunAllSpecs()<CR>

"-----------------------------------------------------------------------------
" Ctrl P
"-----------------------------------------------------------------------------

let g:ctrlp_custom_ignore = {
  \ 'dir': '\v[\/](.git|node_modules|target|elm-stuff|deps)'
  \ }

"-----------------------------------------------------------------------------
" NERDTree
"-----------------------------------------------------------------------------

nnoremap <Leader>n :<C-u>NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"-----------------------------------------------------------------------------
" Fugitive
"-----------------------------------------------------------------------------

nnoremap <silent> <leader>gs :<C-u>Gstatus<CR>
nnoremap <leader>gp :<C-u>Gpush<CR>

"-----------------------------------------------------------------------------
" Markdown
"-----------------------------------------------------------------------------

let g:vim_markdown_folding_disabled = 1
