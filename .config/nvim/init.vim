let &packpath = &runtimepath
let mapleader = ' '

"-----------------------------------------------------------------------------
" Plug Setup
"-----------------------------------------------------------------------------

call plug#begin('~/.local/share/nvim/plugged')

Plug 'airblade/vim-gitgutter'
Plug 'alexlafroscia/postcss-syntax.vim', { 'for': ['css'] }
Plug 'chriskempson/base16-vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'christoomey/vim-tmux-runner'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'elixir-lang/vim-elixir', { 'for': ['elixir'] }
Plug 'ElmCast/elm-vim', { 'for': ['elm'] }
Plug 'godlygeek/tabular'
Plug 'guns/vim-sexp'
Plug 'junegunn/vim-plug'
Plug 'kana/vim-textobj-entire'
Plug 'kana/vim-textobj-user'
Plug 'mattn/emmet-vim'
Plug 'mileszs/ack.vim'
Plug 'mxw/vim-jsx'
Plug 'nelstrom/vim-textobj-rubyblock', { 'for': ['ruby'] }
Plug 'neovimhaskell/haskell-vim'
Plug 'pangloss/vim-javascript', { 'for': ['html', 'javascript', 'javascript.jsx'] }
Plug 'plasticboy/vim-markdown', { 'for': ['markdown'] }
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'thoughtbot/vim-rspec'
Plug 'tmux-plugins/vim-tmux'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fireplace', { 'for': ['clojure'] }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-salve'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-ruby/vim-ruby', { 'for': ['ruby'] }
Plug 'wavded/vim-stylus', { 'for': ['stylus'] }

call plug#end()

nnoremap <leader>pc :PlugClean<cr>
nnoremap <leader>pi :PlugInstall<cr>
nnoremap <leader>ps :PlugStatus<cr>
nnoremap <leader>pu :PlugUpdate<cr>
nnoremap <leader>pU :PlugUpgrade<cr>

"-----------------------------------------------------------------------------
" Base16 Settings
"-----------------------------------------------------------------------------

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace = 256
  source ~/.vimrc_background
endif

"-----------------------------------------------------------------------------
" Core Settings
"-----------------------------------------------------------------------------

runtime macros/matchit.vim
set updatetime=250
set number

" highlight search results
set hlsearch
highlight Search cterm=None ctermfg=black ctermbg=yellow
nnoremap <silent> <leader>ch :<C-u>nohlsearch<CR>

" Cursor Line Highlighting
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END
"
" wildmenu & wildmode
set wildmenu
set wildmode=full
highlight WildMenu ctermfg=white ctermbg=blue

"-----------------------------------------------------------------------------
" Ack Settings
"-----------------------------------------------------------------------------

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

nnoremap <leader>s :<C-u>Ack!<space>

"-----------------------------------------------------------------------------
" Pane Management
"-----------------------------------------------------------------------------

" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :<C-u>wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :<C-u>wincmd =<cr>

"-----------------------------------------------------------------------------
" Vim Tmux Runner
"-----------------------------------------------------------------------------

nnoremap <leader>vs :VtrSendLinesToRunner<cr>
nnoremap <leader>va :VtrAttachToPane<cr>
nnoremap <leader>vf :VtrSendFile<cr>
nnoremap <leader>vo :VtrOpenRunner {'orientation': 'h', 'percentage': 50}<cr>
nnoremap <leader>fr :VtrFocusRunner<cr>
nnoremap <leader>pry :VtrOpenRunner {'orientation': 'h', 'percentage': 50, 'cmd': 'pry'}<cr>

"-----------------------------------------------------------------------------
" Vim RSpec
"-----------------------------------------------------------------------------

let g:rspec_command = "call VtrSendCommand('rspec {spec}')"
nnoremap <leader>tf :call RunCurrentSpecFile()<CR>
nnoremap <leader>ts :call RunNearestSpec()<CR>
nnoremap <leader>tl :call RunLastSpec()<CR>
nnoremap <leader>ta :call RunAllSpecs()<CR>

"-----------------------------------------------------------------------------
" Ctrl P
"-----------------------------------------------------------------------------

let g:ctrlp_custom_ignore = {
  \ 'dir': '\v[\/](.git|node_modules|target|elm-stuff|deps)'
  \ }

"-----------------------------------------------------------------------------
" NERDTree
"-----------------------------------------------------------------------------

nnoremap <Leader>n :<C-u>NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"-----------------------------------------------------------------------------
" Fugitive
"-----------------------------------------------------------------------------

nnoremap <silent> <leader>gs :<C-u>Gstatus<CR>
nnoremap <leader>gp :<C-u>Gpush<CR>

"-----------------------------------------------------------------------------
" Markdown
"-----------------------------------------------------------------------------

let g:vim_markdown_folding_disabled = 1
